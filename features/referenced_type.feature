Feature: Referenced type
    In order to reuse type within an avro schema
    As a PHP Developer
    I need to be able to use references of predefined custom types

    Scenario: Successfully reference a type
        Given the following "message.avsc" file:
        """
        {
            "namespace": "com.jaumo",
            "name": "Message",
            "type": "record",
            "fields": [
                {
                    "name": "foo",
                    "type": { "namespace": "com.jaumo.enum", "name": "Type", "type": "enum", "symbols": ["A", "B"] }
                },
                {
                    "name": "bar",
                    "type": "com.jaumo.enum.Type"
                }
            ]
        }
        """
        When I execute "generate message.avsc"
        Then file "/build/Com/Jaumo/Message.php" should contain:
        """
        <?php

        /**
         * Autogenerated by jaumo/phavroc
         *
         * DO NOT EDIT DIRECTLY
         */
        declare (strict_types=1);
        namespace Com\Jaumo;

        final class Message implements \Phavroc\AvroType
        {
            /** @var null|\Avro\Model\Schema\Schema */
            private static $schema;
            /** @var \Com\Jaumo\Enum\Type */
            private $foo;
            /** @var \Com\Jaumo\Enum\Type */
            private $bar;
            public static function getSchema() : \Avro\Model\Schema\Schema
            {
                if (null === self::$schema) {
                    self::$schema = \Avro\Serde::parseSchema('{"name":"Message","type":"record","fields":[{"name":"foo","type":{"name":"Type","type":"enum","symbols":["A","B"],"namespace":"com.jaumo.enum"}},{"name":"bar","type":"com.jaumo.enum.Type"}],"namespace":"com.jaumo"}');
                }
                return self::$schema;
            }
            public function getFoo() : \Com\Jaumo\Enum\Type
            {
                return $this->foo;
            }
            public function withFoo(\Com\Jaumo\Enum\Type $foo) : self
            {
                $self = clone $this;
                $self->foo = $foo;
                return $self;
            }
            public function getBar() : \Com\Jaumo\Enum\Type
            {
                return $this->bar;
            }
            public function withBar(\Com\Jaumo\Enum\Type $bar) : self
            {
                $self = clone $this;
                $self->bar = $bar;
                return $self;
            }
            private function setFoo(\Com\Jaumo\Enum\Type $foo) : void
            {
                $this->foo = $foo;
            }
            private function setBar(\Com\Jaumo\Enum\Type $bar) : void
            {
                $this->bar = $bar;
            }
            public function normalize() : array
            {
                return ['foo' => $this->foo->normalize(), 'bar' => $this->bar->normalize()];
            }
            public static function denormalize($record) : self
            {
                if (!is_array($record)) {
                    throw new \InvalidArgumentException('Records can only be deserialized from array');
                }
                $self = new self();
                $self->setFoo(isset($record['foo']) ? \Com\Jaumo\Enum\Type::denormalize($record['foo']) : null);
                $self->setBar(isset($record['bar']) ? \Com\Jaumo\Enum\Type::denormalize($record['bar']) : null);
                return $self;
            }
        }
        """
        And file "/build/Com/Jaumo/Enum/Type.php" should exist

    Scenario: Successfully self reference
        Given the following "message.avsc" file:
        """
        {
            "namespace": "com.jaumo",
            "name": "Node",
            "type": "record",
            "fields": [
                {
                    "name": "parent",
                    "type": ["null", "Node"]
                }
            ]
        }
        """
        When I execute "generate message.avsc"
        Then file "/build/Com/Jaumo/Node.php" should contain:
        """
        <?php

        /**
         * Autogenerated by jaumo/phavroc
         *
         * DO NOT EDIT DIRECTLY
         */
        declare (strict_types=1);
        namespace Com\Jaumo;

        final class Node implements \Phavroc\AvroType
        {
            /** @var null|\Avro\Model\Schema\Schema */
            private static $schema;
            /** @var null|Node */
            private $parent;
            public static function getSchema() : \Avro\Model\Schema\Schema
            {
                if (null === self::$schema) {
                    self::$schema = \Avro\Serde::parseSchema('{"name":"Node","type":"record","fields":[{"name":"parent","type":["null","Node"]}],"namespace":"com.jaumo"}');
                }
                return self::$schema;
            }
            public function getParent() : ?Node
            {
                return $this->parent;
            }
            public function withParent(?Node $parent) : self
            {
                $self = clone $this;
                $self->parent = $parent;
                return $self;
            }
            private function setParent(?Node $parent) : void
            {
                $this->parent = $parent;
            }
            public function normalize() : array
            {
                return ['parent' => null !== $this->parent ? $this->parent->normalize() : null];
            }
            public static function denormalize($record) : self
            {
                if (!is_array($record)) {
                    throw new \InvalidArgumentException('Records can only be deserialized from array');
                }
                $self = new self();
                $self->setParent(isset($record['parent']) ? Node::denormalize($record['parent']) : null);
                return $self;
            }
        }
        """
