<?php

/**
 * Copyright 2019 Jaumo GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

use Assert\Assert;
use Assert\Assertion;
use Assert\InvalidArgumentException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Phavroc\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\BufferedOutput;

class FeatureContext implements Context
{
    private $application;
    private $projectDir;
    private $object;

    public function __construct()
    {
        $tempFile = \tempnam(\sys_get_temp_dir(), '');
        if (!\is_string($tempFile)) {
            throw new \RuntimeException('Unable to generate unique filename');
        }

        if (\file_exists($tempFile)) {
            \unlink($tempFile);
        }

        \mkdir($tempFile);
        \chdir($tempFile);
        $this->projectDir = $tempFile;
    }

    /**
     * @BeforeStep
     */
    public function createApplication()
    {
        $this->application = new Application('test');
        $this->application->setAutoExit(false);
    }

    /**
     * @Given the following :filename file:
     */
    public function theFollowingFile($filename, PyStringNode $content)
    {
        $filename = $this->getPrefixedPath($filename);
        $directory = \dirname($filename);
        if (!\is_dir($directory)) {
            \mkdir($directory, 0777, true);
        }
        \file_put_contents($filename, (string) $content);
    }

    /**
     * @Then file :filename should contain:
     */
    public function fileShouldContain($filename, PyStringNode $content)
    {
        $filename = $this->getPrefixedPath($filename);
        $this->fileShouldExist($filename);
        try {
            Assert::that(\file_get_contents($filename))->contains((string) $content);
        } catch (InvalidArgumentException $e) {
            $expected = $content->getStrings();
            $actual = \explode("\n", \file_get_contents($filename));
            $renderer = (new Diff_Renderer_Text_Unified());
            echo (new Diff($expected, $actual))->render($renderer);

            throw $e;
        }
    }

    /**
     * @Then file :filename should exist
     */
    public function fileShouldExist($filename)
    {
        $filename = $this->getPrefixedPath($filename);
        Assertion::file($filename);
    }

    /**
     * @When I execute :command
     */
    public function execute($command)
    {
        $input = new ArgvInput(\explode(' ', $command));
        $output = new BufferedOutput();
        $exitCode = $this->application->run($input, $output);

        if (0 !== $exitCode) {
            throw new \RuntimeException(\sprintf(
                '"%s" returned errored exit code %d: %s',
                $command,
                $exitCode,
                $output->fetch()
            ));
        }
    }

    /**
     * @When I load :className with following data:
     * @param string $className
     * @param PyStringNode $content
     */
    public function loadWithData(string $className, PyStringNode $content): void
    {
        $filename = 'build/' . \str_replace('\\', '/', $className) . '.php';

        /** @noinspection PhpIncludeInspection */
        require_once $this->getPrefixedPath('build/Phavroc/AvroType.php');

        /** @noinspection PhpIncludeInspection */
        require_once $this->getPrefixedPath($filename);

        $loader = "$className::denormalize";
        $data = \json_decode($content->getRaw(), true);

        $this->object = $loader($data);
    }

    /**
     * @Then :getter should return :type :value
     * @param string $getter
     * @param string $type
     * @param string $value
     */
    public function getterShouldReturn(string $getter, string $type, string $value): void
    {
        if ($this->object === null) {
            throw new RuntimeException('No object loaded');
        }

        $actualValue = $this->object->$getter();

        switch ($type) {
            case 'int':
            case 'integer':
                $actualValue = (int) $actualValue;
                break;
            case 'float':
                $actualValue = (float) $actualValue;
                break;
            case 'double':
                $actualValue = (float) $actualValue;
                break;
            case 'string':
                break;
            case 'rfc3339-extended-date':
                $actualValue = $actualValue->format(DateTimeInterface::RFC3339_EXTENDED);
                break;
            default:
                throw new RuntimeException("Unknown type $type");
        }

        if ($value !== $actualValue) {
            throw new RuntimeException("$actualValue does not match expected $value");
        }
    }

    private function getPrefixedPath($filename)
    {
        if (0 === \strpos($filename, $this->projectDir)) {
            return $filename;
        }

        return \sprintf('%s/%s', $this->projectDir, \ltrim($filename, DIRECTORY_SEPARATOR));
    }
}
