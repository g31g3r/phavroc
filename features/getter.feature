Feature: Test getters which require more complex conversions

  Scenario: Test timestamp-micros
    Given the following "message.avsc" file:
        """
        {
            "type": "record",
            "namespace": "com.jaumo",
            "name": "Message",
            "fields": [
                {
                    "name": "expires_at",
                    "type": { "type": "long", "logicalType": "timestamp-micros" }
                }
            ]
        }
        """
    When I execute "generate message.avsc"
    And I load "Com\Jaumo\Message" with following data:
      """
      {
          "expires_at": 1558513214000000
      }
      """
    Then "getExpiresAt" should return "rfc3339-extended-date" "2019-05-22T08:20:14.000+00:00"