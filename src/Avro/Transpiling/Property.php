<?php

/**
 * Copyright 2019 Jaumo GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Phavroc\Avro\Transpiling;

final class Property
{
    private $name;
    private $type;
    private $doc;
    private $nullable;
    private $hasDefaultValue;
    private $defaultValue;

    private function __construct(
        PropertyName $name,
        PropertyType $type,
        ?string $doc
    ) {
        $this->name = $name;
        $this->type = $type;
        $this->doc = $doc;
        $this->nullable = false;
        $this->hasDefaultValue = false;
    }

    public static function fromAvroType(
        PropertyName $name,
        PropertyType $type,
        ?string $doc
    ): self {
        return new self($name, $type, $doc);
    }

    public function withDefaultValue($defaultValue): self
    {
        $self = clone $this;
        $self->hasDefaultValue = true;
        $self->defaultValue = $defaultValue;

        return $self;
    }

    public function withNullable(bool $nullable): self
    {
        $self = clone $this;
        $self->nullable = $nullable;

        return $self;
    }

    public function withType(string $type): self
    {
        $self = clone $this;
        $self->type = $self->type->withValue($type);

        return $self;
    }

    public function phpName(): string
    {
        return $this->name->toPhpValue();
    }

    public function avroName(): string
    {
        return $this->name->toAvroValue();
    }

    public function type(): string
    {
        return $this->type->value();
    }

    public function doc(): ?string
    {
        return $this->doc;
    }

    public function scalar(): bool
    {
        return $this->type->scalar();
    }

    public function hasDefaultValue(): bool
    {
        return $this->hasDefaultValue;
    }

    public function defaultValue()
    {
        return $this->defaultValue;
    }

    public function nullable(): bool
    {
        return $this->nullable;
    }

    public function combinable(): bool
    {
        return $this->type->combinable();
    }

    public function logicalType(): ?string
    {
        return $this->type->logicalType() ? (string) $this->type->logicalType() : null;
    }
}
