<?php

/**
 * Copyright 2019 Jaumo GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Phavroc\Avro\Loader;

final class LoaderError extends \RuntimeException
{
    public static function cannotLoadFile(string $path, ?string $reason = null): self
    {
        $message = \sprintf('Cannot load Avro schema "%s"', $path);
        if (null !== $reason) {
            $message .= \sprintf(' (Reason: %s)', $reason);
        }

        return new self($message);
    }
}
