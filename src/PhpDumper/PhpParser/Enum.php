<?php

/**
 * Copyright 2019 Jaumo GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Phavroc\PhpDumper\PhpParser;

use InvalidArgumentException;
use Phavroc\Avro\Transpiling\Class_ as TranspiledClass;
use Phavroc\Avro\Transpiling\Enum as EnumClass;
use PhpParser\Builder\Method;
use PhpParser\Builder\Param;
use PhpParser\Builder\Property;
use PhpParser\BuilderFactory;
use PhpParser\Node\Arg;
use PhpParser\Node\Const_;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ArrayDimFetch;
use PhpParser\Node\Expr\ArrayItem;
use PhpParser\Node\Expr\Assign;
use PhpParser\Node\Expr\BinaryOp\BooleanAnd;
use PhpParser\Node\Expr\BinaryOp\Identical;
use PhpParser\Node\Expr\BinaryOp\NotIdentical;
use PhpParser\Node\Expr\BooleanNot;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Expr\FuncCall;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\New_;
use PhpParser\Node\Expr\PropertyFetch;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Name\FullyQualified;
use PhpParser\Node\NullableType;
use PhpParser\Node\Scalar\LNumber;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassConst;
use PhpParser\Node\Stmt\Expression;
use PhpParser\Node\Stmt\If_;
use PhpParser\Node\Stmt\Return_;
use PhpParser\Node\Stmt\Throw_;

final class Enum implements NodesProvider
{
    private $builderFactory;

    public function __construct(BuilderFactory $builderFactory = null)
    {
        $this->builderFactory = $builderFactory ?? new BuilderFactory();
    }

    public function supports(TranspiledClass $class): bool
    {
        return $class instanceof EnumClass;
    }

    public function getNodes(TranspiledClass $class): array
    {
        if (!$class instanceof EnumClass) {
            return [];
        }

        $nodes = [];
        $arrayItems = [];
        foreach ($class->options() as $key => $value) {
            $arrayItems[] = new ArrayItem(new LNumber($key), new String_($value));
        }

        $nodes[] = new ClassConst([new Const_('OPTIONS', new Array_($arrayItems))], Class_::MODIFIER_PUBLIC);
        $nodes[] = (new Property('name'))->makePrivate()->setDocComment('/** @var string */');
        $nodes[] = (new Property('value'))->makePrivate()->setDocComment('/** @var int */');
        $nodes[] = (new Method('__construct'))
            ->makePublic()
            ->addParam((new Param('name'))->setType('string'))
            ->addStmt(new Expression(new Assign(
                new PropertyFetch(new Variable('this'), new Identifier('name')),
                new Variable('name')
            )))
            ->addStmt(new Expression(new Assign(
                new PropertyFetch(new Variable('this'), new Identifier('value')),
                new ArrayDimFetch(
                    new ClassConstFetch(new Name('self'), new Identifier('OPTIONS')),
                    new Variable('name')
                )
            )));

        foreach ($class->options() as $option) {
            $prepend = 0 === \strpos($option, '_');
            $append = \strlen($option) - 1 === \strrpos($option, '_');

            $name = \str_replace('_', '', \lcfirst(\ucwords(\strtolower(\trim($option, '_')), '_')));

            if ($prepend) {
                $name = '_' . $name;
            }

            if ($append) {
                $name .= '_';
            }
            $nodes[] = (new Method($name))
                ->makePublic()
                ->makeStatic()
                ->setReturnType('self')
                ->addStmt(new Return_(new New_(new Name('self'), [
                    new Arg(new String_($option)),
                ])));
        }

        $nodes[] = (new Method('fromValue'))
            ->makePublic()
            ->makeStatic()
            ->addParam((new Param('value'))->setType('int'))
            ->addStmts([
                new If_(
                    new Identical(
                        $this->builderFactory->constFetch('false'),
                        new Assign(
                            new Variable('name'),
                            $this->builderFactory->funcCall('array_search', [
                                new Variable('value'),
                                $this->builderFactory->classConstFetch('self', 'OPTIONS'),
                                $this->builderFactory->constFetch('true'),
                            ])
                        )
                    ),
                    [
                        'stmts' => [
                            new Throw_(
                                new New_(new FullyQualified('InvalidArgumentException'), $this->builderFactory->args([
                                    $this->builderFactory->funcCall('sprintf', [
                                        new String_('Option with value "%d" does not exist'),
                                        new Variable('value'),
                                    ]),
                                ]))
                            ),
                        ],
                    ]
                ),
                new Return_(new New_(new Name('self'), [
                    new Arg(new Variable('name')),
                ])),
            ])
            ->setReturnType('self');

        $nodes[] = (new Method('equals'))
            ->makePublic()
            ->setReturnType('bool')
            ->addParam((new Param('other'))->setType(new NullableType($class->name())))
            ->addStmt(new Return_(new BooleanAnd(
                new NotIdentical(
                    new Variable('other'),
                    new ConstFetch(new Name('null'))
                ),
                new BooleanAnd(
                    new Identical(
                        new FuncCall(new FullyQualified('get_class'), [new Arg(new Variable('this'))]),
                        new FuncCall(new FullyQualified('get_class'), [new Arg(new Variable('other'))])
                    ),
                    new Identical(
                        new PropertyFetch(new Variable('this'), new Identifier('name')),
                        new PropertyFetch(new Variable('other'), new Identifier('name'))
                    )
                )
            )));

        $nodes[] = (new Method('getName'))
            ->makePublic()
            ->setReturnType('string')
            ->addStmt(new Return_(new PropertyFetch(new Variable('this'), 'name')));

        $nodes[] = (new Method('getValue'))
            ->makePublic()
            ->setReturnType('int')
            ->addStmt(new Return_(new PropertyFetch(new Variable('this'), 'value')));

        $nodes[] = (new Method('normalize'))
            ->makePublic()
            ->setReturnType('string')
            ->addStmt(new Return_(new MethodCall(new Variable('this'), 'getName')));

        $checkType = new If_(new BooleanNot(new FuncCall(new Name('is_string'), [new Arg(new Variable('name'))])));
        $checkType->stmts = [
            new Throw_(new New_(new Name('\\' . InvalidArgumentException::class), [new Arg(new String_('Enums can only be deserialized from strings'))])),
        ];

        $nodes[] = (new Method('denormalize'))
            ->makePublic()
            ->makeStatic()
            ->setReturnType('self')
            ->addParam(new Param('name'))
            ->addStmt($checkType)
            ->addStmt(new Return_(new New_(new Name('self'), [new Arg(new Variable('name'))])));

        return $nodes;
    }
}
